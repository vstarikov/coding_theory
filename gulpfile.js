var lr = require('tiny-lr'), // Минивебсервер для livereload
    gulp = require('gulp'), // Gulp JS
    livereload = require('gulp-livereload'), // Livereload для Gulp
    connect = require('connect'), // Webserver
    watch = require('gulp-watch'),
    server = lr();

gulp.task('html', function() {
    gulp.src('src/*.html')
        .pipe(gulp.dest('public/'))
        .pipe(livereload(server))
});

gulp.task('css', function() {
    gulp.src('src/css/*.css')
        .pipe(gulp.dest('public/css'))
        .pipe(livereload(server))
});

gulp.task('js', function() {
    gulp.src('src/js/**/*.js')
        .pipe(gulp.dest('public/js'))
        .pipe(livereload(server))
});

gulp.task('http-server', function() {
    connect()
        .use(require('connect-livereload')())
        .use(connect.static('public/'))
        .listen('9000');

    console.log('Server listening on http://localhost:9000');
});



/* Предварительная сборка проектаб его просмотр и запуск на сервере */
gulp.task('watch', function() {
    /* Предварительная сборка проекта */
    gulp.run('html');
    gulp.run('css');
    gulp.run('js');


    livereload.listen();

    /* Подключаем Livereload */
/*    server.listen(35729, function(err) {
        if (err) return console.log(err);
*/
        gulp.watch('src/*.html', function() {
            gulp.run('html');
        });
        gulp.watch('src/css/*.css', function() {
            gulp.run('css');
        });
        gulp.watch('src/js/**/*.js', function() {
            gulp.run('js');
        });
/*    });*/
    gulp.run('http-server');
});



/* Сборка проекта */
gulp.task('build', function() {
    /* html */
    gulp.src('src/*.html')
        .pipe(gulp.dest('build/'));

    /* css */
    gulp.src('src/css/*.css')
        .pipe(gulp.dest('build/css'));

    /* js */
    gulp.src('src/js/**/*.js')
        .pipe(concat('index.js'))
        .pipe(gulp.dest('build/js'));

});