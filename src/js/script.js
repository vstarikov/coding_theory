"use strict";

if (window.File && window.FileReader && window.FileList && window.Blob) {

} else {
    alert('The File APIs are not fully supported in this browser.');
}

var authors = {};

function handleFileSelect(evt) {

    var files = evt.target.files;
    for(var i = 0, f; f = files[i]; i++) {

        var reader = new FileReader();
        reader.onload = (function(theFile) {
            return function(e) {
                var text = e.target.result.split('\n').join(' ').split(' ');

                var author = new Author(text);
                author.delSymbols();
                author.toStr();

                authors[theFile.name] = author;
                console.log('done');
            };
        })(f);
        reader.readAsText(f);
    }
}
document.getElementById('file').addEventListener('change', handleFileSelect, false);

function Author(text){
    this.text = text;
    this.stringText;
    this.bigrams;

    this.delSymbols = function() {
        var arr = this.text;

        /*arr = arr.filter(el => el.charAt(0).toUpperCase() !== el.charAt(0));*/
        arr = arr.filter(function(el) {
            return !(el.charAt(0).toUpperCase() == el.charAt(0));
        });

        for(var i = 0; i < arr.length; i++) {
            for(var j = 0; j<arr[i].length; j++) {
                if ( (arr[i][j].charCodeAt(0) < 'а'.charCodeAt(0) || arr[i][j].charCodeAt(0) > 'я'.charCodeAt(0) )
                    && (arr[i][j].charCodeAt(0) !== 'ё'.charCodeAt(0)) ) {
                    arr[i] = arr[i].replace(arr[i][j], '');
                    j--;
                }
            }
        }

        return this.text = arr.slice();
    };

    this.toStr = function(/*kilobytes*/){
       /* if(kilobytes == undefined) kilobytes = .05;
        kilobytes *= 8*1024;*/

        /*this.stringText = this.text.join('').substring(getRandom(0,this.stringText.length - kilobytes ), kilobytes);*/
        this.stringText = this.text.join('').substr(0/*, kilobytes*/); /* 20кб текста */
        return this.stringText;
    };

    /* сделать глобальной */
    /* должна возвращать матрицы биграм */
    this.findBigrams = function() {
        var arr = this.stringText.split('');
        var bigrams = {};
        for (var i = 0; i < arr.length-1; i++) {
            var bigr = arr[i]+arr[i+1];
            if(bigr in bigrams) {
                bigrams[bigr]++;
            } else{
                bigrams[bigr] = 0;
                bigrams[bigr]++;
            }
        }
        return this.bigrams = bigrams;
    };
}

function getRandom(min, max){
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function getBigrams() {
    var result = [];
    for(var key in authors) {
        result.push('<h3>' + key + '</h3>' );
        var out = authors[key];
        for(var may in out) {
            result.push('<li>', may + ': ' + out[may], '</li>' );
        }
    }

    document.getElementById('bigrams').innerHTML = '<ul>' + result.join('') + '</ul>';
    console.log(authors);
}

function getStringTexts() {
    var result = [];
    for(var key in texts) {
        result.push('<h3>' + key + '</h3>' );
        result.push('<p>' + texts[key] + '</p>');
    }
    document.getElementById('texts').innerHTML = result.join('');
}


/*
После загрузки файлов на обработку на выходе получаем текста авторов

Для того, чтобы пробежаться по всем текстам авторов, в браузере в консоли нужно прописать:
Object.keys(authors).forEach((key, i) => console.log(authors[key].stringText))

Чтобы посчитать биграмы (Например для файла 'Обман.txt', то пишем):
authors['Обман.txt'].findBigrams()

То есть у меня есть конструктор Autror и соответствующие ему методы, которые указываются через this
После загрузки файлов создается объект, элементы которого как раз могут использовать методы, которые я описал выше
*/

/*
Object.keys(authors).forEach((key, i) => console.log(authors[key].stringText))

Object.keys(authors).forEach(function(key) {
    console.log(authors[key].stringText)
})
*/